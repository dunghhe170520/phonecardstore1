var prevScrollpos = window.scrollY;
window.onscroll = function () {
    // Get the element by its ID
    var navbar = document.getElementById('navbar');

    // Get the height of the element
    var height = navbar.offsetHeight;
    
    var currentScrollPos = window.scrollY;
    if (prevScrollpos > currentScrollPos) {
        navbar.style.top = "0";
    } 
    else 
    {
        navbar.style.top = "-"+height+"px";
    }
    prevScrollpos = currentScrollPos;
};