<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%-- 
    Document   : home
    Created on : May 14, 2023, 2:43:21 AM
    Author     : Minh Nguyen
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title>Phone Card Store</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://kit.fontawesome.com/59b37168e9.js" crossorigin="anonymous"></script>
        <script src="/js/myjs.js"></script>
        <link rel="stylesheet" href="/css/mycss.css"/>
        <script>
            var processingNoti = "<div class=\"notification alert alert-"+ "info" +" alert-dismissible fade show\">"+
                                "<button type=\"button\" class=\"btn-close\" data-bs-dismiss=\"alert\"></button>" +
                                "Processing your order..." +
                        "</div>";
            var loadingSupList = "<li class=\"line\" style=\"width: 150px; height:85px;\"></li><li class=\"line\" style=\"width: 150px; height:85px;\"></li><li class=\"line\" style=\"width: 150px; height:85px;\"></li><li class=\"line\" style=\"width: 150px; height:85px;\"></li>";
            var loadingPriceList = "<li class=\"line\" style=\"width: 150px; height:50px; border:none;\"></li><li class=\"line\" style=\"width: 150px; height:50px; border:none;\"></li><li class=\"line\" style=\"width: 150px; height:50px; border:none;\"></li><li class=\"line\" style=\"width: 150px; height:50px; border:none;\"></li>";
            $(document).ready(function () {
                $("#supList").html(loadingSupList);
                $("#buyBtnContainer").html(null);
                $.get("/api/card", {requestStr: "supList"}, function (responseJSON) {
                    $("#supList").html(null);
                    var len = responseJSON.length;
                    for (var i = 0; i < len; i++)
                    {
                        var li_str = "<li><a id=\"" + responseJSON[i].id + "\"><img src=\"" + responseJSON[i].image + "\" alt=\"" + responseJSON[i].name + "\"></a></li>";
                        $("#supList").append(li_str);
                    }
                });
            });
            $(document).on("click", "#supList li", function () {
                $("#supList .active").removeClass("active");
                $("#buyBtnContainer").html(null);
                $(this).addClass("active");
                $("#priceList").html(loadingPriceList);
                $.get("/api/card", {requestStr: "priceList", id: $(this).find("a").attr("id")}, function (responseJSON) {
                    $("#priceList").html(null);
                    var len = responseJSON.length;
                    for (var i = 0; i < len; i++)
                    {
                        var li_str = "<li><a id=\"" + responseJSON[i] + "\">" + responseJSON[i] + ".000₫</a></li>";
                        $("#priceList").append(li_str);
                    }
                });
                $("#quantity").attr({
                    "max": "",
                    "min": "",
                    "value": ""
                });
                $('#quantity').prop('disabled', true);
                $("#stockNumber").text(null);
            });
            $(document).on("click", "#priceList li", function () {
                $("#buyBtnContainer").html("<button type=\"button\" id=\"buyBtn\" class=\"btn btn-primary mt-3\">Buy</button>");
                $("#priceList .active").removeClass("active");
                $(this).addClass("active");
                $('#quantity').prop('disabled', false);
                $("#stockNumber").text(null);
                $.get("/api/card", {requestStr: "quantity", price: $(this).find("a").attr("id"), supId: $("#supList .active").find("a").attr("id")}, function (response) {
                    $("#quantity").attr({
                        "max": response,
                        "min": response === "0" ? "0" : "1",
                        "value": response === "0" ? "0" : "1"
                    });
                    if (response === "0")
                    {
                        $('#quantity').prop('disabled', true);
                        $("#buyBtnContainer").html(null);
                    }
                    $("#stockNumber").text(response + " in stock")
                });
            });
            $(document).on("click", "#buyBtn", function () {
                $("#notificationDiv").html(processingNoti);
                $.get("/api/buy", {price: $("#priceList .active").find("a").attr("id"), supId: $("#supList .active").find("a").attr("id"), quantity: $("#quantity").val() }, function(responseHTML){
                    $("#notificationDiv").html(responseHTML);  
                });
            });
        </script>
    </head>
    <body>
        <nav class="navbar navbar-expand-sm bg-dark navbar-dark" id="navbar">
            <div class="container">
                <a class="navbar-brand" href="/">Phone Card Store</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#collapsibleNavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="collapsibleNavbar">
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown">PROVIDER</a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="#">Viettel</a></li>
                                <li><a class="dropdown-item" href="#">Vinaphone</a></li>
                                <li><a class="dropdown-item" href="#">MobiFone</a></li>
                                <li><a class="dropdown-item" href="#">Vietnamobile</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown">PRICE</a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="#">10.000₫</a></li>
                                <li><a class="dropdown-item" href="#">20.000₫</a></li>
                                <li><a class="dropdown-item" href="#">50.000₫</a></li>
                                <li><a class="dropdown-item" href="#">100.000₫</a></li>
                                <li><a class="dropdown-item" href="#">200.000₫</a></li>
                                <li><a class="dropdown-item" href="#">500.000₫</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="navbar-nav ms-auto">
                        <c:if test="${not empty sessionScope.account}">
                            <span class="navbar-text me-1">${sessionScope.account.getBalance()}000₫</span>
                        </c:if>
                        <c:if test="${sessionScope.account.getIsAdmin()}">
                            <li class="nav-item">
                                <a class="nav-link" href="/admin"><i class="fa-solid fa-gear"></i></i></a>
                            </li>
                        </c:if>
                        <li class="nav-item">
                            <a class="nav-link" href="/profile"><i class="fa-solid fa-circle-user"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/orders"><i class="fa-solid fa-cart-shopping"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <main>
            <section class="container mt-5">
                <div>
                    <h3 class="mb-3">Choose Provider</h3>
                    <ul class="provider__list-container" id="supList">

                    </ul>
                </div>
                <div class="mt-3">
                    <h3 class="mb-3">Choose Price</h3>
                    <ul class="price__list-container" id="priceList">

                    </ul>
                </div>
                <div class="mt-3">
                    <h3 class="mb-3">Choose Quantity</h3>
                    <input type="number" id="quantity" class="form-control" style="width: 200px; display: inline" disabled> <span id="stockNumber"></span>
                </div>
                <c:choose>
                    <c:when test="${not empty sessionScope.account}">
                        <div id="buyBtnContainer">
                        
                        </div>
                    </c:when>
                    <c:otherwise>
                        <h3 class="mt-3">Login to continue</h3>
                    </c:otherwise>
                </c:choose>
            </section>
        </main>
        <footer class="text-center text-lg-start bg-light text-muted">
            <section class="d-flex justify-content-center justify-content-lg-between p-4 border-bottom">
                <div class="me-5 d-none d-lg-block">
                    <span>Get connected with us on social networks:</span>
                </div>
                <div>
                    <a href="https://www.facebook.com/duc.minh.2911/" target="_blank" class="me-4 text-reset">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                </div>
            </section>
            <section class="">
                <div class="container text-center text-md-start mt-5">
                    <div class="row mt-3">
                        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
                            <h6 class="text-uppercase fw-bold mb-4">
                                <i class="fas fa-gem me-3"></i>Phone Card Store
                            </h6>
                            <p>
                                Made by group 6.
                            </p>
                        </div>
                        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
                            <h6 class="text-uppercase fw-bold mb-4">
                                Provider
                            </h6>
                            <p>
                                <a href="#!" class="text-reset">Viettel</a>
                            </p>
                            <p>
                                <a href="#!" class="text-reset">Vinaphone</a>
                            </p>
                            <p>
                                <a href="#!" class="text-reset">MobiFone</a>
                            </p>
                            <p>
                                <a href="#!" class="text-reset">Vietnamobile</a>
                            </p>
                        </div>
                        <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
                            <h6 class="text-uppercase fw-bold mb-4">
                                Useful links
                            </h6>
                            <p>
                                <a href="#!" class="text-reset">Profile</a>
                            </p>
                            <p>
                                <a href="#!" class="text-reset">All products</a>
                            </p>
                            <p>
                                <a href="#!" class="text-reset">Orders</a>
                            </p>
                            <p>
                                <a href="#!" class="text-reset">Cart</a>
                            </p>
                        </div>
                        <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
                            <h6 class="text-uppercase fw-bold mb-4">Contact</h6>
                            <p>
                                <i class="fas fa-envelope me-3"></i>group6swp@outlook.com
                            </p>
                        </div>
                    </div>
                </div>
            </section>
            <div class="text-center p-4" style="background-color: rgba(0, 0, 0, 0.05);">
                © 2023 Copyright:
                <a class="text-reset fw-bold" href="http://dum1.us-east-1.elasticbeanstalk.com/">Phone Card Store</a>
            </div>
        </footer>
        <div id="notificationDiv">
            <c:if test="${not empty mess}">
                <div class="notification alert alert-${alertType} alert-dismissible fade show">
                    <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
                    ${mess}
                </div>
                <c:remove var="mess" scope="session" />
                <c:remove var="alertType" scope="session" />
            </c:if>
        </div>
    </body>
</html>
