/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import com.captcha.botdetect.web.servlet.Captcha;
import dao.AccountDAO;
import dao.DBContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Minh Nguyen
 */
public class ForgotPassword extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet NewPassword</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet NewPassword at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if(session.getAttribute("account")!=null){
            response.sendRedirect("/password");
        }
        else{
            request.getRequestDispatcher("/WEB-INF/forgotpassword.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //AsyncContext asyncContext = request.startAsync(request, response);
        
        Connection con = DBContext.getConnection();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
	HttpSession session = request.getSession();	
        Captcha captcha = Captcha.load(request, "exampleCaptcha");
        boolean isHuman = captcha.validate(request.getParameter("captchaCode"));
        if(isHuman)
        {
            //DBContext b = new DBContext();
            //Connection con = b.getConnection();
            //PreparedStatement pstmt;
            String email = request.getParameter("txtemail");

            Calendar currentTimeNow = Calendar.getInstance();
            currentTimeNow.add(Calendar.MINUTE, 5);
            Date fiveMinsFromNow = currentTimeNow.getTime();

            try 
            {
                pstmt = con.prepareStatement("SELECT changePassExpire FROM account WHERE email=?");
                pstmt.setString(1, email);

                rs = pstmt.executeQuery();
                if (rs.next()) {
                    Date expire = rs.getTimestamp(1);
                    if (fiveMinsFromNow.compareTo(expire) <= 0) 
                    {
                        DBContext.closeConnection(rs, pstmt, con);
                        session.setAttribute("mess", "You have to wait 5 minutes before you can resend the mail."); 
                        session.setAttribute("alertType", "warning");
                        response.sendRedirect("/");
                    } 
                    else if (fiveMinsFromNow.compareTo(expire) > 0) 
                    {
                        //sendChangePassKeyEmail(asyncContext,email);
                        AccountDAO dao = new AccountDAO();
                        dao.updateChangePassKey(email);
                        DBContext.closeConnection(rs, pstmt, con);
                        session.setAttribute("mess", "We have sent an email to <strong>"+email+"</strong>.");
                        session.setAttribute("alertType", "info");
                        response.sendRedirect("/"); 
                    }
                } 
                else 
                {
                    DBContext.closeConnection(rs, pstmt, con);
                    session.setAttribute("mess", "<strong>"+email+"</strong> does not exist.");
                    session.setAttribute("alertType", "danger");
                    response.sendRedirect("/");
                }
            }
            catch (IOException | SQLException e) 
            {
                response.sendRedirect("/");
            }
        }
        else
        {
            session.setAttribute("mess", "You missed the Captcha.");
            session.setAttribute("alertType", "danger");
            response.sendRedirect("/password/new");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    /*private void sendChangePassKeyEmail(AsyncContext asyncContext, String email) {
        new Thread(() -> {
            // Call the email service to send the verification email
            AccountDAO dao = new AccountDAO();
            dao.updateChangePassKey(email);
            asyncContext.complete();
        }).start();
    }*/
}
