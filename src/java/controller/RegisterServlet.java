/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import com.captcha.botdetect.web.servlet.Captcha;
import dao.AccountDAO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Random;
import javax.servlet.http.HttpSession;
import model.Account;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author Minh Nguyen
 */
public class RegisterServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet RegisterServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet RegisterServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
        HttpSession session = request.getSession();
        if(session.getAttribute("account")!=null){
            response.sendRedirect("profile");
        }
        else{
            request.getRequestDispatcher("/WEB-INF/register.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //String gRecaptchaResponse = request.getParameter("h-captcha-response");
	//boolean verify = CaptchaVerify.verify(gRecaptchaResponse);
        Captcha captcha = Captcha.load(request, "exampleCaptcha");
        boolean isHuman = captcha.validate(request.getParameter("captchaCode"));
        String email = request.getParameter("txtemail").trim();
        boolean isExisted;
        HttpSession session = request.getSession();
        if(isHuman)
        {
            AccountDAO dao = new AccountDAO();
            isExisted = dao.checkExisted(email);
            if(isExisted)
            {
                session.setAttribute("mess", "This email had been registered.");
                response.sendRedirect("register");
                //request.setAttribute("mess", "This email had been registered.");
                //request.getRequestDispatcher("register.jsp").forward(request, response);
            }
            else
            {
                String username = request.getParameter("txtusername").trim();
                String firstname = request.getParameter("txtfirstname").trim();
                String lastname = request.getParameter("txtlastname").trim();
                String password = request.getParameter("txtpassword");
                dao.register(username, firstname, lastname, email, password);
                Account acc = dao.getProfile(email);
                dao.updateVerifyKey(email);
                session.setAttribute("account", acc);
                session.setAttribute("mess", "We have sent a verification mail to <strong>"+email+"</strong>.");
                session.setAttribute("alertType", "info");
                response.sendRedirect("/");
            }
        }
        else
        {
            session.setAttribute("mess", "You missed the Captcha.");
            session.setAttribute("alertType", "danger");
            response.sendRedirect("register");
            //request.setAttribute("mess", "You missed the Captcha.");
            //request.getRequestDispatcher("register.jsp").forward(request, response);
        }
    }   
    

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
