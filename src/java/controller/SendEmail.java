/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendEmail {

    private String userEmail;
    private String hash;
    private int type;
    
    public SendEmail(String userEmail, String hash, int type) {
        super();
        this.userEmail = userEmail;
        this.hash = hash;
        this.type = type;
    }

    public void sendMail() {
        String email = System.getProperty("SENDER_EMAIL")!=null?System.getProperty("SENDER_EMAIL"):System.getenv("SENDER_EMAIL");
        String password = System.getProperty("SENDER_PASSWORD")!=null?System.getProperty("SENDER_PASSWORD"):System.getenv("SENDER_PASSWORD");

        Properties properties = new Properties();

        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", "outlook.office365.com");
        properties.put("mail.smtp.port", "587");
        
        Session session = Session.getDefaultInstance(properties, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {

                return new PasswordAuthentication(email, password);
            }
        });

        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(email));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(userEmail));
            if(type == 1)
            {
                message.setSubject("Verify Your Account");
                message.setText("Verification Link :: " + "http://dum1.ap-southeast-1.elasticbeanstalk.com/AccountVerify?key1=" + userEmail + "&key2=" + hash);
                Transport.send(message);
            }
            else if(type == 2)
            {
                message.setSubject("Change your Password");
                message.setText("Click this link to change your password :: " + "http://dum1.ap-southeast-1.elasticbeanstalk.com/password/edit?key1=" + userEmail + "&key2=" + hash);
                Transport.send(message);
            }

        } catch (Exception e) {

            e.printStackTrace();
        }
    }

}
