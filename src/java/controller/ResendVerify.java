/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.AccountDAO;
import dao.DBContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Account;

/**
 *
 * @author Minh Nguyen
 */
public class ResendVerify extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ResendVerify</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ResendVerify at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Account curacc = (Account) session.getAttribute("account");
        Connection con = DBContext.getConnection();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        if (curacc == null) {
            DBContext.closeConnection(rs, pstmt, con);
            response.sendRedirect("/");
        }
        else if(curacc.getIsVerified())
        {
            DBContext.closeConnection(rs, pstmt, con);
            redirect(request,response,"This account had already been verified.","warning","/");
        }
        else
        {
            boolean ajax = "XMLHttpRequest".equals(request.getHeader("X-Requested-With"));
            //DBContext b = new DBContext();
            //Connection con = b.getConnection();
            //PreparedStatement pstmt;
            String email = curacc.getEmail();
            Calendar currentTimeNow = Calendar.getInstance();
            currentTimeNow.add(Calendar.MINUTE, 5);
            Date fiveMinsFromNow = currentTimeNow.getTime();
            response.setContentType("text/html");
            try 
            {
                pstmt = con.prepareStatement("SELECT verifyExpire FROM account WHERE email=?");
                pstmt.setString(1, email);

                rs = pstmt.executeQuery();
                if (rs.next()) {
                    Date expire = rs.getTimestamp(1);
                    if (fiveMinsFromNow.compareTo(expire) <= 0) 
                    {
                        DBContext.closeConnection(rs, pstmt, con);
                        if (ajax) 
                        {
                            response.getWriter().println(responseNotiHTML("You have to wait 5 minutes before you can resend the mail.","warning"));
                        } 
                        else 
                        {
                            redirect(request,response,"You have to wait 5 minutes before you can resend the mail.","warning","/profile");
                        }
                    } 
                    else if (fiveMinsFromNow.compareTo(expire) > 0) 
                    {
                        DBContext.closeConnection(rs, pstmt, con);
                        AccountDAO dao = new AccountDAO();
                        dao.updateVerifyKey(email);
                        if (ajax) 
                        {
                            response.getWriter().println(responseNotiHTML("We have sent an email to <strong>" + email + "</strong>.","info"));
                        } 
                        else 
                        {
                            redirect(request,response,"We have sent an email to <strong>" + email + "</strong>.","info","/profile");
                        }
                    }
                } 
                else 
                {
                    DBContext.closeConnection(rs, pstmt, con);
                    if (ajax) 
                    {
                        response.getWriter().println(responseNotiHTML("<strong>" + email + "</strong> does not exist.","danger"));
                    } 
                    else 
                    {
                        redirect(request,response,"<strong>" + email + "</strong> does not exist.","danger","/");
                    }
                }
            } 
            catch (IOException | SQLException e) 
            {
                if (ajax) 
                {
                    response.getWriter().println(responseNotiHTML("Something went wrong.","danger"));
                } 
                else 
                {
                    response.sendRedirect("/");
                }
            }
            /*try {
                Thread.sleep(2000);
                response.getWriter().println(responseNotiHTML("Testing","success"));
            } catch (InterruptedException ex) {
                Logger.getLogger(ResendVerify.class.getName()).log(Level.SEVERE, null, ex);
            }*/
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    private void redirect(HttpServletRequest request, HttpServletResponse response, String mess, String alertType, String url)
            throws ServletException, IOException
    {
        HttpSession session = request.getSession();
        session.setAttribute("mess", mess);
        session.setAttribute("alertType", alertType);
        response.sendRedirect(url);
    }
    private String responseNotiHTML(String mess, String alertType)
    {
        String html=    "<div class=\"notification alert alert-"+ alertType +" alert-dismissible fade show\">\n"+
                                "<button type=\"button\" class=\"btn-close\" data-bs-dismiss=\"alert\"></button>\n" +
                                mess + "\n" +
                        "</div>";
        return html;
    }
}
