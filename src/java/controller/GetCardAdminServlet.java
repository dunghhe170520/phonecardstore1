/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import com.google.gson.Gson;
import dao.ProductDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Account;
import model.Product;

/**
 *
 * @author Minh Nguyen
 */
public class GetCardAdminServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet GetCardAdminServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet GetCardAdminServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String requestStr = request.getParameter("requestStr");
        String limit = request.getParameter("limit");
        String pageNumber = request.getParameter("pageNumber");
        String deleteId = request.getParameter("deleteId");
        ProductDAO dao = new ProductDAO();
        if(requestStr.equalsIgnoreCase("list"))
        {
            ArrayList<Product> list = dao.getCardsWithPagination(limit,pageNumber);
            Object[] objects = list.toArray();
            Gson gson = new Gson();
            String json = gson.toJson(objects);
            response.setContentType("application/json");
            response.getWriter().write(json);
        }
        else if(requestStr.equalsIgnoreCase("numofpage"))
        {
            String totalPages = dao.getNumberOfPages(limit);
            response.setContentType("text/plain");
            response.getWriter().write(totalPages);
        }
        else if(requestStr.equalsIgnoreCase("delete"))
        {
            response.setContentType("text/html");
            if(dao.deleteCard(deleteId))
            {
                response.getWriter().write(responseNotiHTML("Deleted", "success"));
            }
            else
            {
                response.getWriter().write(responseNotiHTML("Cannot be deleted", "danger"));
            }
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String requestStr = request.getParameter("requestStr");
        String title = request.getParameter("title");
        String description = request.getParameter("description");
        String price = request.getParameter("price");
        String supId = request.getParameter("supId");
        
        ProductDAO dao = new ProductDAO();
        if(requestStr.equalsIgnoreCase("addCard"))
        {
            Account acc = (Account) session.getAttribute("account");
            response.setContentType("text/html");
            if(dao.addCard(title,description,price,acc.getId(),supId))
            {
                response.getWriter().write(responseNotiHTML("Added", "success"));
            }
            else
            {
                response.getWriter().write(responseNotiHTML("Cannot be added", "danger"));
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    private String responseNotiHTML(String mess, String alertType)
    {
        String html=    "<div class=\"notification alert alert-"+ alertType +" alert-dismissible fade show\">\n"+
                                "<button type=\"button\" class=\"btn-close\" data-bs-dismiss=\"alert\"></button>\n" +
                                mess + "\n" +
                        "</div>";
        return html;
    }
}
