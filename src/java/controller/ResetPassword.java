/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.AccountDAO;
import dao.DBContext;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Account;
import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author Minh Nguyen
 */
public class ResetPassword extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ChangePassword</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ChangePassword at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String email = request.getParameter("key1");
        String hash = request.getParameter("key2");

        Connection con = DBContext.getConnection();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        HttpSession session = request.getSession();
        
        try {
            pstmt = con.prepareStatement("SELECT changePassExpire FROM account WHERE email=? AND changePassKey=?");
            pstmt.setString(1, email);
            pstmt.setString(2, hash);
            
            rs = pstmt.executeQuery();
            if (rs.next()){
                Date expire = rs.getTimestamp(1);
                Date current = new Date();
                if(current.compareTo(expire) <= 0) 
                {
                    DBContext.closeConnection(rs, pstmt, con);
                    request.setAttribute("txtemail", email);
                    request.getRequestDispatcher("/WEB-INF/resetpassword.jsp").forward(request, response);
                } 
                else if (current.compareTo(expire) > 0) 
                {
                    DBContext.closeConnection(rs, pstmt, con);
                    session.setAttribute("mess", "Your password token is expired.");
                    session.setAttribute("alertType", "danger");
                    response.sendRedirect("/");
                } 
            }
            else
            {
                DBContext.closeConnection(rs, pstmt, con);
                session.setAttribute("mess", "Invalid Link.");
                session.setAttribute("alertType", "danger");
                response.sendRedirect("/");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String email = request.getParameter("txtemail");
        String password = request.getParameter("txtpassword");
        AccountDAO dao = new AccountDAO();
        dao.resetPassword(email,password);
        session.invalidate();
        //session.setAttribute("mess", "Password has been changed.");
        response.sendRedirect("/login");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
