/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.Date;

/**
 *
 * @author Minh Nguyen
 */
public class Account 
{
    private int id;
    private String email;
    private boolean isAdmin;
    private String username;
    private String firstname;
    private String lastname;
    private boolean isVerified;
    private int balance;
    //Constructor
    public Account(){
    
    }
    public Account(int id, String email, boolean isAdmin, String username, String firstname, String lastname, boolean isVerified, int balance)
    {
        this.id=id;
        this.email=email;
        this.isAdmin=isAdmin;
        this.username=username;
        this.firstname=firstname;
        this.lastname=lastname;
        this.isVerified=isVerified;
        this.balance=balance;
    }
    //Setter
    public void setId(int id)
    {
        this.id=id;
    }
    public void setEmail(String email)
    {
        this.email=email;
    }
    public void setIsAdmin(boolean isAdmin)
    {
        this.isAdmin=isAdmin;
    }
    public void setUserName(String username)
    {
        this.username=username;
    }
    public void setFirstName(String firstname)
    {
        this.firstname=firstname;
    }
    public void setLastName(String lastname)
    {
        this.lastname=lastname;
    }
    public void setIsVerified(boolean isVerified)
    {
        this.isVerified=isVerified;
    }
    public void setBalance(int balance)
    {
        this.balance=balance;
    }
    //Getter
    public int getId()
    {
        return this.id;
    }
    public String getEmail()
    {
        return this.email;
    }
    public boolean getIsAdmin()
    {
        return this.isAdmin;
    }
    public String getUserName()
    {
        return this.username;
    }
    public String getFirstName()
    {
        return this.firstname;
    }
    public String getLastName()
    {
        return this.lastname;
    }
    public boolean getIsVerified()
    {
        return this.isVerified;
    }    
    public int getBalance()
    {
        return this.balance;
    }
}
