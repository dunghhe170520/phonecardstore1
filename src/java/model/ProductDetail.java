package model;

public class ProductDetail {
    private int id;
    private String productTitle;
    private String seriNumber;
    private String code;
    private String productExpireDate;
    

    public ProductDetail() {
    }

    public ProductDetail(int id, String productTitle, String seriNumber, String code, String productExpireDate) {
        this.id = id;
        this.productTitle = productTitle;
        this.seriNumber = seriNumber;
        this.code = code;
        this.productExpireDate = productExpireDate;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProductTitle() {
        return productTitle;
    }

    public void setProductTitle(String productTitle) {
        this.productTitle = productTitle;
    }   

    public String getSeriNumber() {
        return seriNumber;
    }

    public void setSeriNumber(String seriNumber) {
        this.seriNumber = seriNumber;
    }

    public String getProductExpireDate() {
        return productExpireDate;
    }

    public void setProductExpireDate(String productExpireDate) {
        this.productExpireDate = productExpireDate;
    }
    
}
