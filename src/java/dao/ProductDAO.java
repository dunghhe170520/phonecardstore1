package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.ProductDetail;
import model.Product;


public class ProductDAO extends DBContext {

    public ArrayList<Product> getAllCard() {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        ArrayList<Product> list = new ArrayList<>();
        
        String sql = "select p.id,p.title,p.description,p.price,p.quantity,p.createdBy,s.name from product p inner join supplier s on p.supplier = s.id";
        try {
            statement = conn.prepareStatement(sql);
            rs = statement.executeQuery();
            while (rs.next()) {
                Product p = new Product(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDouble(4), rs.getInt(5), rs.getInt(6), rs.getString(7));
                list.add(p);
            } 
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        return list;
    }
    public ArrayList<Product> getCardsWithPagination(String limit,String pageNumber) 
    {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        ArrayList<Product> list = new ArrayList<>();
        int intLimit = Integer.parseInt(limit);
        int intPage = Integer.parseInt(pageNumber);
        try {
            String sql = "select p.id,p.title,p.description,p.price,p.quantity,p.createdBy,s.name from product p inner join supplier s on p.supplier = s.id order by p.id limit ? offset ?";
            statement = conn.prepareStatement(sql);
            statement.setInt(1, intLimit);
            statement.setInt(2, (intLimit * intPage) - intLimit);
            rs = statement.executeQuery();
            while (rs.next()) {
                Product p = new Product(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDouble(4), rs.getInt(5), rs.getInt(6), rs.getString(7));
                list.add(p);
            }
        }
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        return list;
    }
    public int getQuantityBySupIdAndPrice(String supId,int price) 
    {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        int n = 0;
        try {
            String sql = "select quantity from product where price = ? and supplier = ?";
            statement = conn.prepareStatement(sql);
            statement.setInt(1, price);
            statement.setString(2, supId);
            rs = statement.executeQuery();
            while (rs.next()) {
                n = rs.getInt(1);
                break;
            }
        }
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        return n;
    }
    public int getIdBySupIdAndPrice(String supId,int price) 
    {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        int n = 0;
        try {
            String sql = "select id from product where price = ? and supplier = ?";
            statement = conn.prepareStatement(sql);
            statement.setInt(1, price);
            statement.setString(2, supId);
            rs = statement.executeQuery();
            while (rs.next()) {
                n = rs.getInt(1);
                break;
            }
        }
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        return n;
    }
    public String getNumberOfPages(String limit) 
    {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        int n = -1;
        try {
            String sql = "select CEIL(count(id)/?) from product;";
            statement = conn.prepareStatement(sql);
            statement.setString(1, limit);
            rs = statement.executeQuery();
            while (rs.next()) {
                n = rs.getInt(1);
            }
        }
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        return Integer.toString(n);
    }
    public boolean deleteCard(String id)
    {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        int a=0;
        try 
        {
            String sql0 = "delete from product_detail WHERE (id = ?)";
            statement = conn.prepareStatement(sql0);
            statement.setString(1, id);
            statement.executeUpdate();
            String sql = "delete from product WHERE (id = ?)";
            statement = conn.prepareStatement(sql);
            statement.setString(1, id);
            a = statement.executeUpdate();
            
        }
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        if(a==0)
        {
            return false;
        }
        return true;
    }
    public boolean addCard(String title, String description, String price,int uid,String supId)
    {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        int a = 0;
        try {
            String sql = "insert into product (title, description, price, quantity, createdBy, supplier) values (?,?,?,0,?,?)";
            statement = conn.prepareStatement(sql);
            statement.setString(1, title);
            statement.setString(2, description);
            statement.setString(3, price);
            statement.setInt(4, uid);
            statement.setString(5, supId);
            a = statement.executeUpdate();
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        if(a==0)
        {
            return false;
        }
        return true;
    }

    public Product getProductByIdAndPrice(int id, double price) {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement stm = null;
        ArrayList<Product> list = new ArrayList<>();
        String sql = "SELECT * FROM product p join supplier s on p.supplier = s.id \n"
                + "where s.id = ?  and price = ? ";
        try {
            stm = conn.prepareStatement(sql);
            stm.setInt(1, id);
            stm.setDouble(2, price);
            rs = stm.executeQuery();
            while (rs.next()) {
                Product p = new Product();
                p.setId(rs.getInt("id"));
                p.setTitle(rs.getString("title"));
                p.setDescription(rs.getString("description"));
                p.setPrice(rs.getDouble("price"));
                return p;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        finally 
        {
            closeConnection(rs, stm, conn);
        }
        return null;
    }

    public void insertImg(String img, String pId) {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement stm = null;
        try {
            String sql = "insert into product_detail (image, product_id) values  (?, ?)";
            stm = conn.prepareStatement(sql);
            stm.setString(1, img);
            stm.setString(2, pId);
            stm.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, stm, conn);
        }
    }

    public ArrayList<Float> getAllPrice(String supId) {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        ArrayList<Float> list = new ArrayList<>();
        try {
            String sql = "Select distinct price from product where supplier = ?";
            statement = conn.prepareStatement(sql);
            statement.setString(1, supId);
            rs = statement.executeQuery();
            while (rs.next()) 
            {
                list.add(rs.getFloat("price"));
            }
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        return list;
    }
    
    public String getQuantityByProvAndPrice(String price,String supId) {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        String n = "0";
        try {
            String sql = "Select quantity from product where price = ? and supplier = ?";
            statement = conn.prepareStatement(sql);
            statement.setString(1, price);
            statement.setString(2, supId);
            rs = statement.executeQuery();
            while (rs.next()) 
            {
                n = rs.getString(1);
            }
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        return n;
    }
}
