/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import controller.SendEmail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import model.Account;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.dbutils.DbUtils;

/**
 *
 * @author Minh Nguyen
 */
public class AccountDAO extends DBContext 
{
    private final ExecutorService executorService = Executors.newSingleThreadExecutor();
    public ArrayList<Account> getAllAccounts() 
    {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        ArrayList<Account> list = new ArrayList<>();
        try {
            String sql = "SELECT id,email,isAdmin,username,firstname,lastname,isVerified,balance FROM account;";
            statement = conn.prepareStatement(sql);
            rs = statement.executeQuery();
            while (rs.next()) {
                Account a = new Account(rs.getInt(1),rs.getString(2),rs.getBoolean(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getBoolean(7),rs.getInt(8));
                list.add(a);
            }
        }
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        return list;
    }
    
    public boolean checkLogin(String email, String password) 
    {
        boolean isValid = false;
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        try {
            String passwordHash = DigestUtils.sha256Hex(password);
            String sql = "select email,passwordHash from account where email = ? and passwordHash = ?";
            statement = conn.prepareStatement(sql);
            statement.setString(1, email);
            statement.setString(2, passwordHash);
            rs = statement.executeQuery();
            isValid = rs.next() ? true : false;
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        return isValid;
    }

    public Account getProfile(String email) {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        Account acc = null;
        try {
            String sql = "select id,isAdmin,username,firstname,lastname,isVerified,balance from account where email = ?";
            statement = conn.prepareStatement(sql);
            statement.setString(1, email);
            rs = statement.executeQuery();
            while (rs.next()) {
                acc = new Account(rs.getInt(1),email,rs.getBoolean(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getBoolean(6),rs.getInt(7));
                break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        return acc;
    }
    
    public Account getProfileByID(int id) {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        Account acc = null;
        try {
            String sql = "select email,isAdmin,username,firstname,lastname,isVerified,balance from account where id = ?";
            statement = conn.prepareStatement(sql);
            statement.setInt(1, id);
            rs = statement.executeQuery();
            while (rs.next()) {
                acc = new Account(id,rs.getString(1),rs.getBoolean(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getBoolean(6),rs.getInt(7));
                break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        return acc;
    }

    public void register(String username, String firstname, String lastname, String email, String password) {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        try {
            String passwordHash = DigestUtils.sha256Hex(password);
            Calendar cal = Calendar.getInstance();
            Timestamp createdOn = new Timestamp(cal.getTimeInMillis());
            String sql = "insert into account (email, passwordHash, isAdmin, username, firstname, lastname, isVerified, createdOn, updatedOn, verifyKey, verifyExpire, changePassKey, changePassExpire, balance) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            statement = conn.prepareStatement(sql);
            statement.setString(1, email);
            statement.setString(2, passwordHash);
            statement.setString(3, "0");
            statement.setString(4, username);
            statement.setString(5, firstname);
            statement.setString(6, lastname);
            statement.setString(7, "0");
            statement.setString(8, createdOn.toString());
            statement.setString(9, createdOn.toString());
            statement.setString(10, "");
            statement.setString(11, createdOn.toString());
            statement.setString(12, "");
            statement.setString(13, createdOn.toString());
            statement.setInt(14, 0);
            statement.executeUpdate();
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
    }

    public void updateProfile(int id, String username, String firstname, String lastname) {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        try {
            Calendar cal = Calendar.getInstance();
            Timestamp current = new Timestamp(cal.getTimeInMillis());
            String sql = "update account set username = ?, firstname = ?, lastname = ?, updatedOn = ? WHERE (id = ?)";
            statement = conn.prepareStatement(sql);
            statement.setString(1, username);
            statement.setString(2, firstname);
            statement.setString(3, lastname);
            statement.setString(4, current.toString());
            statement.setInt(5, id);
            statement.executeUpdate();
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
    }

    public void updatePassword(int id, String password) 
    {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        PreparedStatement statement1 = null;
        try {
            String passwordHash = DigestUtils.sha256Hex(password);
            Calendar cal = Calendar.getInstance();
            Timestamp changePassExpire = new Timestamp(cal.getTimeInMillis());
            String sql = "update account set passwordHash = ?, changePassExpire = ?, updatedOn = ?  WHERE (id = ?);";
            statement = conn.prepareStatement(sql);
            statement.setString(1, passwordHash);
            statement.setString(2, changePassExpire.toString());
            statement.setString(3, changePassExpire.toString());
            statement.setInt(4, id);
            statement.executeUpdate();
            
            sql = "delete from account_auth WHERE (account_id = ?);";
            statement1 = conn.prepareStatement(sql);
            statement1.setInt(1, id);
            statement1.executeUpdate();
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
            closeConnection(rs, statement1, conn);
        }
    }
    
    public void resetPassword(String email, String password) 
    {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        try {
            String passwordHash = DigestUtils.sha256Hex(password);
            Calendar cal = Calendar.getInstance();
            Timestamp changePassExpire = new Timestamp(cal.getTimeInMillis());
            String sql = "update account set passwordHash = ?, changePassExpire = ?, updatedOn = ?  WHERE (email = ?)";
            statement = conn.prepareStatement(sql);
            statement.setString(1, passwordHash);
            statement.setString(2, changePassExpire.toString());
            statement.setString(3, changePassExpire.toString());
            statement.setString(4, email);
            statement.executeUpdate();
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
    }
    
    public void updateVerifyKey(String email) 
    {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        try {
            Random random = new Random();
            random.nextInt(999999);
            String verifyKey = DigestUtils.sha256Hex(""+random);
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.MINUTE, 10);
            Timestamp verifyExpire = new Timestamp(cal.getTimeInMillis());
            String sql = "update account set verifyKey = ?, verifyExpire = ?  WHERE (email = ?)";
            statement = conn.prepareStatement(sql);
            statement.setString(1, verifyKey);
            statement.setString(2, verifyExpire.toString());
            statement.setString(3, email);
            //executorService
            Runnable sendmailrun = () -> {
                SendEmail se = new SendEmail(email, verifyKey,1);
                se.sendMail();
            };
            executorService.submit(sendmailrun);
            executorService.shutdown();
            //SendEmail se = new SendEmail(email, verifyKey,1);
            //se.sendMail();
            statement.executeUpdate();
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
    }
    
    public void updateChangePassKey(String email) {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        try {
            Random random = new Random();
            random.nextInt(999999);
            String changePassKey = DigestUtils.sha256Hex(""+random);
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.MINUTE, 10);
            Timestamp changePassExpire = new Timestamp(cal.getTimeInMillis());
            String sql = "update account set changePassKey = ?, changePassExpire = ?  WHERE (email = ?)";
            statement = conn.prepareStatement(sql);
            statement.setString(1, changePassKey);
            statement.setTimestamp(2, changePassExpire);
            statement.setString(3, email);
            Runnable sendmailrun = () -> {
                SendEmail se = new SendEmail(email, changePassKey,2);
                se.sendMail();
            };
            executorService.submit(sendmailrun);
            executorService.shutdown();
            //SendEmail se = new SendEmail(email, changePassKey,2);
            //se.sendMail();
            statement.executeUpdate();
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
    }

    public void verify(String email) 
    {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        try {
            String sql = "update account set isVerified = '1', verifyExpire = ?, updatedOn = ? WHERE (email = ?)";
            Calendar cal = Calendar.getInstance();
            Timestamp verifyExpire = new Timestamp(cal.getTimeInMillis());
            statement = conn.prepareStatement(sql);
            statement.setString(1, verifyExpire.toString());
            statement.setString(2, verifyExpire.toString());
            statement.setString(3, email);
            statement.executeUpdate();
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
    }

    public boolean checkExisted(String email) 
    {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        boolean isExisted = false;
        try {
            String sql = "select email from account where email = ?";
            statement = conn.prepareStatement(sql);
            statement.setString(1, email);
            rs = statement.executeQuery();
            isExisted = rs.next() ? true : false;
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        return isExisted;
    }
    
    public int getNumberOfAccounts() 
    {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        int n = -1;
        try {
            String sql = "select count(id) from account;";
            statement = conn.prepareStatement(sql);
            rs = statement.executeQuery();
            while (rs.next()) 
            {
                n = rs.getInt(1);
            }
        }
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        return n;
    }
    
    public String getNumberOfPages(String limit) 
    {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        int n = -1;
        try {
            String sql = "select CEIL(count(id)/?) from account;";
            statement = conn.prepareStatement(sql);
            statement.setString(1, limit);
            rs = statement.executeQuery();
            while (rs.next()) {
                n = rs.getInt(1);
            }
        }
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        return Integer.toString(n);
    }
    public ArrayList<Account> getAccountsWithPagination(String limit,String pageNumber) 
    {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        ArrayList<Account> list = new ArrayList<>();
        int intLimit = Integer.parseInt(limit);
        int intPage = Integer.parseInt(pageNumber);
        try {
            String sql = "SELECT id,email,isAdmin,username,firstname,lastname,isVerified,balance FROM account limit ? offset ?;";
            statement = conn.prepareStatement(sql);
            statement.setInt(1, intLimit);
            statement.setInt(2, (intLimit * intPage) - intLimit);
            rs = statement.executeQuery();
            while (rs.next()) {
                Account a = new Account(rs.getInt(1),rs.getString(2),rs.getBoolean(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getBoolean(7),rs.getInt(8));
                list.add(a);
            }
        }
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        return list;
    }
    public boolean deactivateAccount(String id)
    {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        int a=0;
        try 
        {
            String sql = "delete from account WHERE (id = ? and isAdmin = 0)";
            statement = conn.prepareStatement(sql);
            statement.setString(1, id);
            a = statement.executeUpdate();
        }
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        if(a==0)
        {
            return false;
        }
        return true;
    }
    
    public int getBalanceByID(int id) {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        int n = -1;
        try {
            String sql = "select balance from account where id = ?";
            statement = conn.prepareStatement(sql);
            statement.setInt(1, id);
            rs = statement.executeQuery();
            while (rs.next()) {
                n = rs.getInt(1);
                break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        return n;
    }
    
    public void setBalanceByID(int id,int balance) {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        try {
            String sql = "UPDATE account SET balance = ? WHERE (id = ?)";
            statement = conn.prepareStatement(sql);
            statement.setInt(1, balance);
            statement.setInt(2, id);
            statement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
    }
}
