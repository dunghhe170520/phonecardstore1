/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.ProductDetail;

/**
 *
 * @author Minh Nguyen
 */
public class ProductDetailDAO extends DBContext {
    public ArrayList<ProductDetail> getCardsDetailWithPagination(String limit,String pageNumber) 
    {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        ArrayList<ProductDetail> list = new ArrayList<>();
        int intLimit = Integer.parseInt(limit);
        int intPage = Integer.parseInt(pageNumber);
        try {
            String sql = "select pd.id, p.title, pd.seriNumber,pd.code,pd.productExpireDate from product_detail pd inner join product p on pd.product_id = p.id order by pd.id limit ? offset ?";
            statement = conn.prepareStatement(sql);
            statement.setInt(1, intLimit);
            statement.setInt(2, (intLimit * intPage) - intLimit);
            rs = statement.executeQuery();
            while (rs.next()) {
                ProductDetail pd = new ProductDetail(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));
                list.add(pd);
            }
        }
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        return list;
    }
    public ArrayList<ProductDetail> getCardsDetailWithPIDAndLimit(String limit,int PId) 
    {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        ArrayList<ProductDetail> list = new ArrayList<>();
        int intLimit = Integer.parseInt(limit);
        try {
            String sql = "select pd.id, p.title, pd.seriNumber,pd.code,pd.productExpireDate from product_detail pd inner join product p on pd.product_id = p.id where p.id = ? order by pd.id asc limit ?";
            statement = conn.prepareStatement(sql);
            statement.setInt(1, PId);
            statement.setInt(2, intLimit);
            rs = statement.executeQuery();
            while (rs.next()) {
                ProductDetail pd = new ProductDetail(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));
                list.add(pd);
            }
            String sql1 = "delete from product_detail where product_id = ? order by id asc limit ?";
            statement = conn.prepareStatement(sql1);
            statement.setInt(1, PId);
            statement.setInt(2, intLimit);
            statement.executeUpdate();
        }
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        return list;
    }
    public String getNumberOfPages(String limit) 
    {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        int n = -1;
        try {
            String sql = "select CEIL(count(id)/?) from product_detail;";
            statement = conn.prepareStatement(sql);
            statement.setString(1, limit);
            rs = statement.executeQuery();
            while (rs.next()) {
                n = rs.getInt(1);
            }
        }
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        return Integer.toString(n);
    }
    public boolean addCardDetail(String proId, String seriNumber,String code,String expiration)
    {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        int a = 0;
        try {
            String sql = "insert into product_detail (product_id, seriNumber, code, productExpireDate) values (?,?,?,?)";
            statement = conn.prepareStatement(sql);
            statement.setString(1, proId);
            statement.setString(2, seriNumber);
            statement.setString(3, code);
            statement.setString(4, expiration);
            a = statement.executeUpdate();
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        if(a==0)
        {
            return false;
        }
        return true;
    }
    public boolean deleteCardDetail(String id)
    {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        int a=0;
        try 
        {
            String sql = "delete from product_detail WHERE (id = ?)";
            statement = conn.prepareStatement(sql);
            statement.setString(1, id);
            a = statement.executeUpdate();
        }
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        if(a==0)
        {
            return false;
        }
        return true;
    }
}
