/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import static dao.DBContext.getConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.Supplier;


public class SupplierDAO extends DBContext
{
     public Supplier getSupplierById(String pid) {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        Supplier p = new Supplier();
        try 
        {
            String sql = " Select * from supplier where id = " + pid;
            statement = conn.prepareStatement(sql);
            rs = statement.executeQuery();
            while (rs.next()) 
            {
                p = new Supplier(rs.getInt(1),rs.getString(2),rs.getString(3));
            }
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        return p;
    }
     
     public ArrayList<Supplier> getAllSupplier() {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        ArrayList<Supplier> list = new ArrayList<>();
        try 
        {
            String sql = "Select * from supplier";
            statement = conn.prepareStatement(sql);
            rs = statement.executeQuery();
            while (rs.next()) {
                Supplier p = new Supplier(rs.getInt(1), rs.getString(2), rs.getString(3));
                list.add(p);
            }
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        return list;
    }
     
}
