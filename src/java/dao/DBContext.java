/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.*;
import java.sql.DriverManager;
import org.apache.commons.dbutils.DbUtils;



public class DBContext 
{
    public DBContext() 
    {
    }
    
    public static void closeConnection(ResultSet rs,PreparedStatement statement,Connection conn)
    {
        DbUtils.closeQuietly(rs);
        DbUtils.closeQuietly(statement);
        DbUtils.closeQuietly(conn);
    }
    public static Connection getConnection() 
    {
        String jdbcUrl = System.getProperty("JDBC_CONNECTION_STRING")!=null?System.getProperty("JDBC_CONNECTION_STRING"):System.getenv("JDBC_CONNECTION_STRING");
        Connection conn = null;
        try 
        {
            Class.forName("com.mysql.cj.jdbc.Driver");
            //String jdbcUrl = "jdbc:mysql://" + "awseb-e-wmb2tsxgz5-stack-awsebrdsdatabase-swprjamwn4mq.cyssgogatecf.ap-southeast-1.rds.amazonaws.com" + ":" + "3306" + "/" + "ebdb" + "?user=" + "root" + "&password=" + "minhaprovip6996";
            conn = DriverManager.getConnection(jdbcUrl);
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        return conn;
    }
}
