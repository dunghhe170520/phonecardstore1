/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import static dao.DBContext.closeConnection;
import static dao.DBContext.getConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.Order;

/**
 *
 * @author Minh Nguyen
 */
public class OrderDAO extends DBContext {
    public void addOrder(int proId, int UID, String productLog, int totalPrice) {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        try {
            String sql = "INSERT INTO `order` (product_id, createdBy, productLog, totalPrice) VALUES (?,?,?,?)";
            statement = conn.prepareStatement(sql);
            statement.setInt(1, proId);
            statement.setInt(2, UID);
            statement.setString(3, productLog);
            statement.setInt(4, totalPrice);
            statement.executeUpdate();
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
    }
    public ArrayList<Order> getAllOrdersByUID(int UID) 
    {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        ArrayList<Order> list = new ArrayList<>();
        try {
            String sql = "select * from `order` where createdBy = ?";
            statement = conn.prepareStatement(sql);
            statement.setInt(1, UID);
            rs = statement.executeQuery();
            while (rs.next()) {
                Order o = new Order(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getString(4), rs.getInt(5));
                list.add(o);
            }
        }
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        return list;
    }
    public int getNumberOfOrders() 
    {
        Connection conn = getConnection();
        ResultSet rs = null;
        PreparedStatement statement = null;
        int n = -1;
        try {
            String sql = "select count(id) from `order`;";
            statement = conn.prepareStatement(sql);
            rs = statement.executeQuery();
            while (rs.next()) 
            {
                n = rs.getInt(1);
            }
        }
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        finally 
        {
            closeConnection(rs, statement, conn);
        }
        return n;
    }
}
