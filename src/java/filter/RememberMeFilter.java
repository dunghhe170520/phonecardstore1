/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package filter;

/**
 *
 * @author Minh Nguyen
 */
import dao.AccountAuthDAO;
import dao.AccountDAO;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpSession;
import model.Account;
import model.AccountAuthToken;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.RandomStringUtils;

public class RememberMeFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // Initialization code, if needed
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        HttpSession session = httpRequest.getSession(false);

        boolean loggedIn = session != null && session.getAttribute("account") != null;

        Cookie[] cookies = httpRequest.getCookies();

        if (!loggedIn && cookies != null) {
            // process auto login for remember me feature
            String selector = "";
            String rawValidator = "";

            for (Cookie aCookie : cookies) {
                if (aCookie.getName().equals("selector")) {
                    selector = aCookie.getValue();
                } else if (aCookie.getName().equals("validator")) {
                    rawValidator = aCookie.getValue();
                }
            }

            if (!"".equals(selector) && !"".equals(rawValidator)) {
                AccountAuthDAO authDAO = new AccountAuthDAO();
                AccountAuthToken token = authDAO.getBySelector(selector);
                AccountDAO accDAO=new AccountDAO();
                if (token != null) {
                    String hashedValidatorDatabase = token.getValidator();
                    String hashedValidatorCookie = DigestUtils.sha256Hex(rawValidator);

                    if (hashedValidatorCookie.equals(hashedValidatorDatabase)) {
                        session = httpRequest.getSession();
                        session.setAttribute("account", accDAO.getProfileByID(token.getAccountId()));
                        loggedIn = true;
                        String oldSelector = token.getSelector();
                        // update new token in database
                        String newSelector = RandomStringUtils.randomAlphanumeric(12);
                        String newRawValidator = RandomStringUtils.randomAlphanumeric(64);

                        token.setSelector(newSelector);
                        token.setValidator(newRawValidator);
                        
                        authDAO.update(oldSelector,token);

                        // update cookie
                        Cookie cookieSelector = new Cookie("selector", newSelector);
                        cookieSelector.setMaxAge(7 * 24 * 60 * 60);
                        cookieSelector.setPath("/");
                        
                        Cookie cookieValidator = new Cookie("validator", newRawValidator);
                        cookieValidator.setMaxAge(7 * 24 * 60 * 60);
                        cookieValidator.setPath("/");
                        
                        httpResponse.addCookie(cookieSelector);
                        httpResponse.addCookie(cookieValidator);
                    }
                }
            }
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        // Cleanup code, if needed
    }

    private boolean checkIfUserIsAdmin(HttpServletRequest request) {
        // Implement your logic to check if the user is an admin
        // You can use session attributes, database lookups, or any other means of determining the user's role
        // For example, if you have a session attribute named "isAdmin" set to true for admin users:
        Account curacc = (Account) request.getSession().getAttribute("account");
        if (curacc == null) {
            return false;
        }
        Boolean isAdmin = curacc.getIsAdmin();
        return isAdmin;
    }
}
