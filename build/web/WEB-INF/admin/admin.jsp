<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%-- 
    Document   : home
    Created on : May 14, 2023, 2:43:21 AM
    Author     : Minh Nguyen
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title>Dashboard</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://kit.fontawesome.com/59b37168e9.js" crossorigin="anonymous"></script>
        <script src="/js/myjs.js"></script>
        <link rel="stylesheet" href="/css/mycss.css"/>

    </head>
    <body>
        <nav class="navbar navbar-expand-sm bg-dark navbar-dark" id="navbar">
            <div class="container">
                <a class="navbar-brand" href="/">Phone Card Store</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#collapsibleNavbar">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="collapsibleNavbar">
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown">PROVIDER</a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="#">Viettel</a></li>
                                <li><a class="dropdown-item" href="#">Vinaphone</a></li>
                                <li><a class="dropdown-item" href="#">MobiFone</a></li>
                                <li><a class="dropdown-item" href="#">Vietnamobile</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown">PRICE</a>
                            <ul class="dropdown-menu">
                                <li><a class="dropdown-item" href="#">10.000₫</a></li>
                                <li><a class="dropdown-item" href="#">20.000₫</a></li>
                                <li><a class="dropdown-item" href="#">50.000₫</a></li>
                                <li><a class="dropdown-item" href="#">100.000₫</a></li>
                                <li><a class="dropdown-item" href="#">200.000₫</a></li>
                                <li><a class="dropdown-item" href="#">500.000₫</a></li>
                            </ul>
                        </li>
                    </ul>
                    <ul class="navbar-nav ms-auto">
                        <c:if test="${not empty sessionScope.account}">
                            <span class="navbar-text me-1">${sessionScope.account.getBalance()}000₫</span>
                        </c:if>
                        <c:if test="${sessionScope.account.getIsAdmin()}">
                            <li class="nav-item">
                                <a class="nav-link" href="/admin"><i class="fa-solid fa-gear"></i></i></a>
                            </li>
                        </c:if>
                        <li class="nav-item">
                            <a class="nav-link" href="/profile"><i class="fa-solid fa-circle-user"></i></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/orders"><i class="fa-solid fa-cart-shopping"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <main class="container-fluid page-body-wrapper">
            <nav class="sidebar sidebar-offcanvas" id="sidebar">
                <ul class="nav">
                    <li class="nav-item nav-profile">
                        <a href="/profile" class="nav-link">
                            <i class="fa-solid fa-circle-user" style="font-size: 40px;"></i>
                            <div class="nav-profile-text d-flex flex-column">
                                <span class="fs-4 fw-bold mb-2">${sessionScope.account.getUserName()}</span>
                                <span class="fs-6 text-small">Admin</span>
                            </div>
                        </a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="/admin">
                            <span class="menu-title">Dashboard</span>

                            <i class="fa-solid fa-house menu-icon"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-bs-toggle="collapse" href="#product-nav" aria-expanded="false"
                           aria-controls="product-nav">
                            <span class="menu-title">Products</span>
                            <i class="fa-solid menu-arrow "></i>
                            <i class="fa-solid fa-box-archive menu-icon"></i>
                        </a>
                        <div class="collapse" id="product-nav">
                            <ul class="nav flex-column sub-menu">
                                <li class="nav-item "> <a class="nav-link" href="/admin/product">Product list</a></li>
                                <li class="nav-item "> <a class="nav-link" href="/admin/product/detail">Product detail</a></li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/user">
                            <span class="menu-title">Users</span>
                            <i class="fa-solid fa-users menu-icon"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/order">
                            <span class="menu-title">Orders</span>
                            <i class="fa-solid fa-cart-shopping menu-icon"></i>
                        </a>
                    </li>
                </ul>
            </nav>
            <div class="main-panel">
                <div class="content-wrapper">
                    <div class="page-header">
                        <h3 class="page-title">
                            <span class="page-title-icon bg-gradient-primary text-white me-2">
                                <i class="fa-solid fa-house"></i>
                            </span> Dashboard
                        </h3>
                    </div>
                    <div class="row">
                        <div class="col-md-4 stretch-card grid-margin">
                            <div class="card bg-gradient-danger card-img-holder text-white">
                                <div class="card-body">
                                    <img src="/assets/circle.svg" class="card-img-absolute" alt="circle-image" />
                                    <h4 class="font-weight-normal mb-3">Registered Users <i class="fa-solid fa-user"></i>
                                    </h4>
                                    <h2 class="mb-5">${numOfUsers}</h2>
                                    <h6 class="card-text"></h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 stretch-card grid-margin">
                            <div class="card bg-gradient-info card-img-holder text-white">
                                <div class="card-body">
                                    <img src="/assets/circle.svg" class="card-img-absolute" alt="circle-image" />
                                    <h4 class="font-weight-normal mb-3">Total Orders <i class="fa-solid fa-cart-shopping"></i>
                                    </h4>
                                    <h2 class="mb-5">${numOfOrders}</h2>
                                    <h6 class="card-text"></h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 stretch-card grid-margin">
                            <div class="card bg-gradient-success card-img-holder text-white">
                                <div class="card-body">
                                    <img src="/assets/circle.svg" class="card-img-absolute" alt="circle-image" />
                                    <h4 class="font-weight-normal mb-3">... <i class="mdi mdi-diamond mdi-24px float-right"></i>
                                    </h4>
                                    <h2 class="mb-5">...</h2>
                                    <h6 class="card-text"></h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-7 grid-margin stretch-card">
                            <div class="card">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <h4 class="card-title float-left">...</h4>
                                        <div id="visit-sale-chart-legend" class="rounded-legend legend-horizontal legend-top-right float-right"></div>
                                    </div>
                                    <canvas id="visit-sale-chart" class="mt-4"></canvas>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 grid-margin stretch-card">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">...</h4>
                                    <canvas id="traffic-chart"></canvas>
                                    <div id="traffic-chart-legend" class="rounded-legend legend-vertical legend-bottom-left pt-4"></div>
                                </div>
                            </div>
                        </div>
                    </div>                
                </div>
            </div>
        </main>
        <footer class="text-center text-lg-start bg-light text-muted">
            <section class="d-flex justify-content-center justify-content-lg-between p-4 border-bottom">
                <div class="me-5 d-none d-lg-block">
                    <span>Get connected with us on social networks:</span>
                </div>
                <div>
                    <a href="https://www.facebook.com/duc.minh.2911/" target="_blank" class="me-4 text-reset">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                </div>
            </section>
            <section class="">
                <div class="container text-center text-md-start mt-5">
                    <div class="row mt-3">
                        <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
                            <h6 class="text-uppercase fw-bold mb-4">
                                <i class="fas fa-gem me-3"></i>Phone Card Store
                            </h6>
                            <p>
                                Made by group 6.
                            </p>
                        </div>
                        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
                            <h6 class="text-uppercase fw-bold mb-4">
                                Provider
                            </h6>
                            <p>
                                <a href="#!" class="text-reset">Viettel</a>
                            </p>
                            <p>
                                <a href="#!" class="text-reset">Vinaphone</a>
                            </p>
                            <p>
                                <a href="#!" class="text-reset">MobiFone</a>
                            </p>
                            <p>
                                <a href="#!" class="text-reset">Vietnamobile</a>
                            </p>
                        </div>
                        <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
                            <h6 class="text-uppercase fw-bold mb-4">
                                Useful links
                            </h6>
                            <p>
                                <a href="#!" class="text-reset">Profile</a>
                            </p>
                            <p>
                                <a href="#!" class="text-reset">All products</a>
                            </p>
                            <p>
                                <a href="#!" class="text-reset">Orders</a>
                            </p>
                            <p>
                                <a href="#!" class="text-reset">Cart</a>
                            </p>
                        </div>
                        <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
                            <h6 class="text-uppercase fw-bold mb-4">Contact</h6>
                            <p>
                                <i class="fas fa-envelope me-3"></i>group6swp@outlook.com
                            </p>
                        </div>
                    </div>
                </div>
            </section>
            <div class="text-center p-4" style="background-color: rgba(0, 0, 0, 0.05);">
                © 2023 Copyright:
                <a class="text-reset fw-bold" href="http://dum1.us-east-1.elasticbeanstalk.com/">Phone Card Store</a>
            </div>
        </footer>
        <c:if test="${not empty mess}">
            <div class="notification alert alert-${alertType} alert-dismissible fade show">
                <button type="button" class="btn-close" data-bs-dismiss="alert"></button>
                ${mess}
            </div>
            <c:remove var="mess" scope="session" />
            <c:remove var="alertType" scope="session" />
        </c:if>
    </body>
</html>
